package ru.redsys.log.filter;

import java.io.*;
import java.sql.Timestamp;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Пример фильтра, который сбрасывает информацию о состоянии запроса
 * в соответствующий лог-файл контекста сервлета.
 *
 * Фильтр может использоваться ​​по мере необходимости, чтобы помочь в отладке проблем.
 * Для этого в tomcat в web.xml надо прописать
 * <filter>
 * <filter-name>requestdumper</filter-name>
 * <filter-class>
 * ru.redsys.log.filter.ReqRespDumpFilter
 * </filter-class>
 * </filter>
 * <filter-mapping>
 * <filter-name>requestdumper</filter-name>
 * <url-pattern>*</url-pattern>
 * </filter-mapping>​
 *
 * Created by lebedev on 09.09.16.
 */
public class ReqRespDumpFilter implements Filter {

    /**
     * The filter configuration object we are associated with.  If this value
     * is null, this filter instance is not currently configured.
     */
    private FilterConfig filterConfig = null;

    /**
     * Take this filter out of service.
     */
    public void destroy() {
        this.filterConfig = null;
    }

    /**
     * Time the processing that is performed by all subsequent filters in the
     * current filter stack, including the ultimately invoked servlet.
     *
     * @param request The servlet request we are processing
     * @param response  The servlet response we are creating
     * @param chain   The filter chain we are processing
     * @throws IOException      if an input/output error occurs
     * @throws ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (filterConfig == null)
            return;

        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse)response;

        BufferedRequestWrapper bufferedReqest = new BufferedRequestWrapper(httpServletRequest);
        BufferedResponseWrapper bufferedResponse = new BufferedResponseWrapper(httpServletResponse);

        try {

            filterConfig.getServletContext().log("============== REST REQUEST BEGIN ==============\n");

            final StringBuilder logMessage = new StringBuilder("REST Request - Time ")
                    .append(new Timestamp(System.currentTimeMillis())).append('\n')
                    .append("\n -------------- HEADER SECTION -------------- \n")
                    .append(" [REQUEST HEADERS:");

            Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                String value = httpServletRequest.getHeader(name);
                logMessage.append("\n ").append(name).append(": ").append(value);
            }

            logMessage.append("] \n")
                    .append("\n -------------- GENERAL SECTION -------------- \n")
                    .append(" [HTTP METHOD: ")
                    .append(httpServletRequest.getMethod())
                    .append("] \n [PATH INFO: ")
                    .append(httpServletRequest.getPathInfo())
                    .append("] \n [PATH HOST: ")
                    .append(httpServletRequest.getServerName())
                    .append("] \n [PATH PORT: ")
                    .append(httpServletRequest.getServerPort())
                    .append("] \n [REQUEST BODY: ")
                    .append(bufferedReqest.getRequestBody())
                    .append("] \n [REMOTE ADDRESS: ")
                    .append(httpServletRequest.getRemoteAddr())
                    .append("] \n");

            chain.doFilter(bufferedReqest, bufferedResponse);

            logMessage.append("\n -------------- RESPONSE SECTION -------------- \n")
                    .append(" [RESPONSE: ").append( bufferedResponse.getContent() ).append("] \n");

            filterConfig.getServletContext().log(logMessage.toString());
            filterConfig.getServletContext().log("============== REST REQUEST END ==============\n\n");
        } catch( Throwable a ) {
            a.printStackTrace();
            System.out.println(bufferedResponse.getContent()); // Do your logging job here. This is just a basic example.
        }
    }

    /**
     * Place this filter into service.
     *
     * @param filterConfig The filter configuration object
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    private static final class BufferedRequestWrapper extends HttpServletRequestWrapper {

        private ByteArrayInputStream bais = null;
        private ByteArrayOutputStream baos = null;
        private BufferedServletInputStream bsis = null;
        private byte[] buffer = null;


        public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
            super(req);
            // Read InputStream and store its content in a buffer.
            InputStream is = req.getInputStream();
            this.baos = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int letti;
            while ((letti = is.read(buf)) > 0) {
                this.baos.write(buf, 0, letti);
            }
            this.buffer = this.baos.toByteArray();
        }


        @Override
        public ServletInputStream getInputStream() {
            this.bais = new ByteArrayInputStream(this.buffer);
            this.bsis = new BufferedServletInputStream(this.bais);
            return this.bsis;
        }

        String getRequestBody() throws IOException  {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getInputStream()));
            String line = null;
            StringBuilder inputBuffer = new StringBuilder();
            do {
                line = reader.readLine();
                if (null != line) {
                    inputBuffer.append(line.trim());
                }
            } while (line != null);
            reader.close();
            return inputBuffer.toString().trim();
        }

    }

    private static final class BufferedServletInputStream extends ServletInputStream {

        private ByteArrayInputStream bais;

        public BufferedServletInputStream(ByteArrayInputStream bais) {
            this.bais = bais;
        }

        @Override
        public int available() {
            return this.bais.available();
        }

        @Override
        public int read() {
            return this.bais.read();
        }

        @Override
        public int read(byte[] buf, int off, int len) {
            return this.bais.read(buf, off, len);
        }


        @Override
        public boolean isFinished() {
            return bais.available() == 0;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {

        }
    }

    public class ServletOutputStreamCopier extends ServletOutputStream {

        private OutputStream outputStream;
        private ByteArrayOutputStream copy;

        public ServletOutputStreamCopier(OutputStream outputStream) {
            this.outputStream = outputStream;
            this.copy = new ByteArrayOutputStream(1024);
        }

        @Override
        public void write(int b) throws IOException {
            outputStream.write(b);
            copy.write(b);
        }

        public byte[] getCopy() {
            return copy.toByteArray();
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }
    }

    public class BufferedResponseWrapper extends HttpServletResponseWrapper {

        private ServletOutputStream outputStream;
        private PrintWriter writer;
        private ServletOutputStreamCopier copier;

        public BufferedResponseWrapper(HttpServletResponse response) throws IOException {
            super(response);
        }

        public String getContent() throws UnsupportedEncodingException {
            byte[] copy = getCopy();
            return new String(copy, getResponse().getCharacterEncoding());
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (writer != null) {
                throw new IllegalStateException("getWriter() has already been called on this response.");
            }

            if (outputStream == null) {
                outputStream = getResponse().getOutputStream();
                copier = new ServletOutputStreamCopier(outputStream);
            }

            return copier;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (outputStream != null) {
                throw new IllegalStateException("getOutputStream() has already been called on this response.");
            }

            if (writer == null) {
                copier = new ServletOutputStreamCopier(getResponse().getOutputStream());
                writer = new PrintWriter(new OutputStreamWriter(copier, getResponse().getCharacterEncoding()), true);
            }

            return writer;
        }

        @Override
        public void flushBuffer() throws IOException {
            if (writer != null) {
                writer.flush();
            } else if (outputStream != null) {
                copier.flush();
            }
        }

        public byte[] getCopy() {
            if (copier != null) {
                return copier.getCopy();
            } else {
                return new byte[0];
            }
        }
    }
}
