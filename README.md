# README #

# Каcтомный фильтр для Tomcat #

Пример фильтра, который сбрасывает информацию о состоянии запроса
в соответствующий лог-файл контекста сервлета. 

Фильтр может использоваться ​​по мере необходимости, чтобы помочь в отладке проблем.
Для этого в tomcat в web.xml надо прописать

```
#!xml

  <filter>
    <filter-name>requestdumper</filter-name>
    <filter-class>
      ru.redsys.log.filter.ReqRespDumpFilter
    </filter-class>
  </filter>
  <filter-mapping>
    <filter-name>requestdumper</filter-name>
    <url-pattern>*</url-pattern>
  </filter-mapping>​
```